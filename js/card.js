// tag loading dan data kosong
const createCard = () => {
  document.getElementById(
    "content"
  ).innerHTML = `<div id="list-card" class="row"></div><div class="w-100 text-center mt-3">
    <p id="loading" class="fs-5">Loading....</p>
    <p id="data-kosong" class="fs-5 text-danger px-5 border border-danger"style=" display: none;">Data Kosong</p>
  </div>`;
};

const card = (data) => {
  const listCard = document.getElementById("list-card");
  const loading = document.getElementById("loading");
  const dataKosong = document.getElementById("data-kosong");
  let dataCard = "";
  if (data.length === 0) {
    listCard.innerHTML = dataCard;
    loading.style.display = "none";
    dataKosong.style.display = "";
  } else {
    loading.style.display = "none";
    dataKosong.style.display = "none";
    data.forEach((val) => {
      dataCard += `<div class="col-lg-3 col-md-6 p-1">
        <div class="card">
          <img src=${val.urlToImage} class="card-img-top" alt="" style="height:200px;"/>
          <div class="card-body">
            <h5 class="card-title fs-6">${val.title}</h5>
            <p class="card-text fs-6 text-wrap">
              ${val.description}
            </p>
            <a href=${val.url} class="btn btn-primary">Selengkapnya</a>
          </div>
        </div>
        </div>`;
    });
    listCard.innerHTML = dataCard;
  }
};

export { card, createCard };
