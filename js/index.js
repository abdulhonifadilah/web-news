import { card, createCard } from "./card.js";


// card(newData);
const cbData = (data) => {
  document.getElementById("search").addEventListener("keyup", () => {
    let newData = data.filter((d) => {
      let title = d.title.toLowerCase();
      let filter = document.getElementById("search").value.toLowerCase();
      return title.includes(filter);
    });
    card(newData);
  });
  card(data);
};
function getData(url, cb) {
  axios
    .get(url)
    .then((res) => {
      cb(res.data.articles);
    })
    .catch(() => console.log("error"));
}

let url = "https://newsapi.org/v2/top-headlines?country=id&apiKey=fdd3d8165a8a4a87800cd8ceb85988c9";

createCard();
getData(url, cbData);
